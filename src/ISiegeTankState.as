package  
{
	public interface ISiegeTankState 
	{
		function get damage():Number;
		
		function get color():uint;
		
		function attack():void;
		
		function move(targetX:Number, targetY:Number):void;
		
		function toTankMode():void;

		function toSiegeMode():void;

		function toSpeedMode():void;
	}
}