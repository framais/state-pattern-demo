package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	[SWF(backgroundColor="0xededed", frameRate="60")]

	public class Main extends Sprite 
	{
		private var siegeTank:SiegeTank;
		
		public function Main():void 
		{
			siegeTank = new SiegeTank();
			
			siegeTank.x = 100;
			siegeTank.y = 100;
			
			addChild(siegeTank);
			
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			stage.addEventListener(MouseEvent.CLICK, onClick);
		}

		private function onClick(event:MouseEvent):void
		{
			siegeTank.move(event.stageX, event.stageY);
		}
		
		private function onKeyUp(event:KeyboardEvent):void
		{
			switch (event.keyCode)
			{
				case Keyboard.SPACE:
					siegeTank.attack();
					break;
					
				case 49: // Keyboard 1
					siegeTank.toTankMode();
					break;
					
				case 50: // Keyboard 2
					siegeTank.toSiegeMode();
					break;
					
				case 51: // Keyboard 3
					siegeTank.toSpeedMode();
					break;
			}
		}		
	}	
}