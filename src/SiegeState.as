package  
{
	public class SiegeState implements ISiegeTankState
	{
		private var siegeTank:SiegeTank;
		
		public function SiegeState(siegeTank:SiegeTank) 
		{
			this.siegeTank = siegeTank;
		}
		
		public function get damage():Number
		{
			return 20;
		}
		
		public function get color():uint
		{
			return 0x00cc00;
		}
		
		public function attack():void
		{
			siegeTank.attackTextField.text = "Attacking for " + damage;
		}
		
		public function move(targetX:Number, targetY:Number):void
		{
			siegeTank.moveTextField.text = "Can't move :(";
		}
		
		public function toTankMode():void 
		{
			siegeTank.state = siegeTank.tankState;
		}
		
		public function toSiegeMode():void
		{
			siegeTank.messageTextField.text = "Already in Siege Mode!";
		}
		
		public function toSpeedMode():void 
		{
			siegeTank.messageTextField.text = "Can't go from Siege to Speed!";
		}
	}
}